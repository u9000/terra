//    terra, a human-focused web directory
//    Copyright (C) 2020 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, version 3 of the License.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

use diesel;
use diesel::deserialize;
use diesel::prelude::*;
use diesel::types::FromSql;
use diesel::sql_types::BigInt;
use diesel::backend::Backend;
use diesel::types::ToSql;
use diesel::serialize::{self,Output};

use std::io::Write;

use rocket::request::{self,FromRequest,Request};
use rocket::Outcome;
use rocket::http::Status;
use serde::Deserialize;

use crate::db;
use crate::user::User;

#[derive(Responder,Debug,Deserialize)]
pub struct Token(pub String);

impl<'a, 'r> FromRequest<'a, 'r> for Token {
    type Error = AuthTokenError;

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, Self::Error> {
        let keys: Vec<_> = request.headers().get("X-AUTH").collect();

        // Grab a connection out of rocket's connection pool
        let connection = &*match request.guard::<db::DbConn>(){
            Outcome::Success(value) => value,
            _ => return Outcome::Failure((Status::InternalServerError,AuthTokenError::GuardError)),
        };
        match keys.len() {
            0 => Outcome::Failure((Status::Unauthorized, AuthTokenError::Missing)),
            1 if {if let Ok(res) = is_valid(keys[0],connection) {res} else {false}} => Outcome::Success(Token(keys[0].to_string())),
            1 if is_valid(keys[0],connection).is_err() => Outcome::Failure((Status::InternalServerError, AuthTokenError::DBError)),
            1 => Outcome::Failure((Status::Unauthorized, AuthTokenError::Invalid)),
            _ => Outcome::Failure((Status::BadRequest, AuthTokenError::BadCount)),
        }
    }
}

impl<DB> ToSql<BigInt, DB> for Token
where
    DB: Backend,
    i64: ToSql<BigInt, DB>,
{
    fn to_sql<W: Write>(&self, out: &mut Output<W, DB>) -> serialize::Result {
        self.0.parse::<i64>()?.to_sql(out)
    }
}

#[derive(Debug)]
pub enum AuthTokenError {
    Missing,
    Invalid,
    BadCount,
    DBError,
    GuardError
}

// Check if an auth token is actually valid
fn is_valid(auth: &str, connection: &diesel::PgConnection) -> Result<bool,AuthTokenError> {
    use db::users::dsl::*;

    // Parse the token out to an integer
    if let Ok(token) = auth.parse::<i64>() {
        
        // Check to see if the token is valid for any user
        let results = users.filter(tokens.contains(vec![token]))
            .load::<User>(connection)
            .map_err(|_| AuthTokenError::Invalid)?;

        // If it's valid for exactly 1 user then we're good
        if results.len() == 1 {
            Ok(true)

        // Otherwise the token is invalid
        } else {
            Ok(false)
        }
    } else {
        Err(AuthTokenError::Invalid)
    }


}

impl<DB> FromSql<BigInt, DB> for Token
where
    DB: Backend,
    i64: FromSql<BigInt, DB>,
{
    fn from_sql(bytes: Option<&DB::RawValue>) -> deserialize::Result<Self> {
        Ok(Token(format!("{}",i64::from_sql(bytes)?)))
    }
}
