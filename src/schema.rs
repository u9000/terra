table! {
    registration_codes (code) {
        code -> Int8,
    }
}

table! {
    sites (id) {
        id -> Nullable<Int4>,
        url -> Text,
        title -> Text,
        description -> Text,
        uploader -> Text,
        tags -> Array<Text>,
        post_date -> Timestamp,
    }
}

table! {
    users (username) {
        username -> Varchar,
        password -> Varchar,
        tokens -> Array<Int8>,
    }
}

allow_tables_to_appear_in_same_query!(
    registration_codes,
    sites,
    users,
);
