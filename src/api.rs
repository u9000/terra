//    terra, a human-focused web directory
//    Copyright (C) 2020 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, version 3 of the License.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::db::{self,DbConn};
use crate::token::Token;
use crate::user::PostUser;
use crate::site::{OptionalPostSite,PostSite,Site};
use crate::user::User;
use diesel::prelude::*;
use diesel::dsl::date;
use diesel::result::Error;
use rocket::http::Status;
use bcrypt::{self,BcryptError};
use rand::seq::SliceRandom;

use rand;

use rocket_contrib::json::Json;

use chrono::naive::NaiveDate;

macro_rules! maybe {
    ($e: expr) => {{
        {$e}.map_err(|_| Status::InternalServerError)?
    }}
}

pub type Date = String;

// This is a simple wrapper around bcrypts hash function
// It just calls hash with a default cost
pub fn hash(pass: &str) -> Result<String,BcryptError> {
    let cost = 10;
    bcrypt::hash(pass,cost)
}

// Authenticate as a user
#[post("/auth", format = "application/json", data = "<user>")]
pub fn auth(pooled_conn: db::DbConn, user: Json<PostUser>) -> Result<Token,Status> {
    use db::users::dsl::*;
    let connection = &*pooled_conn;

    // Check to see if the user exists in the database
    let db_user: User = users.filter(username.eq(&user.username))
                             .first(connection)
                             .map_err(|e| match e {
                                         Error::NotFound => Status::Unauthorized,
                                         _ => Status::InternalServerError})?;

    // If the password fails to verify then exit early before authing the user
    if !bcrypt::verify(&user.password,&db_user.password)
        .map_err(|_| Status::InternalServerError)? {
        return Err(Status::Unauthorized);
    }

    
    // If the user has a good password then generate a new token
    // Add it into the db
    // And return it
    
    let mut token_vec: Vec<i64> = db_user.tokens
        .iter()
        .map(|Token(x)| str::parse::<i64>(x)
             .map_err(|_| Status::InternalServerError))
        .collect::<Result<Vec<i64>,Status>>()?;


    // Generate a code
    let new_code = rand::random::<i64>().abs();
    token_vec.push(new_code);

    // Add it to the db
    let rows_inserted = diesel::update(
            users.filter(username.eq(&user.username)))
        .set(tokens.eq(token_vec))
        .execute(connection)
        .map_err(|_| Status::InternalServerError)?;


    // If we couldn't add the token to the db then throw an error
    if rows_inserted != 1 {
        return Err(Status::InternalServerError);
    }

    // And return it
    Ok(Token(new_code.to_string()))
}

// Register a new user
#[post("/register/<sent_code>",format = "application/json", data = "<user>")]
pub fn register(pooled_conn: DbConn, user: Json<PostUser>, sent_code: i64) -> Result<(),Status> {
    let connection = &*pooled_conn;
    // Check if the code is valid
    {
        use db::registration_codes::dsl::*;
        let deleted_rows: usize = diesel::delete(registration_codes.filter(code.eq(sent_code)))
                                .execute(connection)
                                .map_err(|_| Status::InternalServerError)?;
        // If there were no valid codes then exit early
        if deleted_rows != 1 {
            return Err(Status::Unauthorized);
        }
    }
    {
        // If we've reached this point then the code is valid
        // and we can insert the new user into the db
        use db::users::dsl::*;

        diesel::insert_into(users)
            .values(user.into_inner()
                    .to_db()
                    .map_err(|_| Status::InternalServerError)?)
            .execute(connection)
            .map_err(|e| match e {
                Error::DatabaseError(_,_) => Status::Conflict,
                _ => Status::InternalServerError})?;

        Ok(())
    }

}

// Generate a code to give to a new user
//
// The code should be a u64 but we can't enforce that because it's a core type 
// and I can't impl Responder for it
#[get("/new_code")]
pub fn new_code(pooled_conn: DbConn, _key: Token) -> Result<String,Status> {
    use db::registration_codes::dsl::*;

    // Generate a code
    let new_code = rand::random::<i64>().abs();

    // Add it to the db
    let connection = &*pooled_conn;
    let rows_inserted = diesel::insert_into(registration_codes)
        .values(&vec![code.eq(new_code)])
        .execute(connection)
        .map_err(|_| Status::InternalServerError)?;

    // If we couldn't add the code to the db then throw an error
    if rows_inserted != 1 {
        return Err(Status::InternalServerError);
    }

    // And return it
    Ok(new_code.to_string())
}

// Add a new site to the directory
#[post("/add_site", format= "application/json", data = "<site>")]
pub fn add_site(pooled_conn: DbConn, key: Token, site: Json<PostSite>) -> Result<Json<Site>,Status> {
    use db::sites::dsl::*;

    let connection = &*pooled_conn;

    // Get the username from the token we have
    let authed_as: User = {
        use db::users::dsl::*;
        users.filter(tokens.contains(vec![str::parse::<i64>(&key.0).unwrap()]))
             .first(connection)
             .map_err(|_| Status::InternalServerError)?
    };

    // User is already authed and everything so just add the site into the database

    let user = authed_as.username;
    let site = site.into_inner().to_db(user);

    let result = diesel::insert_into(sites)
        .values(&site)
        .get_result(connection)
        .map_err(|_| Status::InternalServerError)?;


    return Ok(Json(result))
}

// Fetch sites from the directory
// This supports a bunch of different filters so servers and clients
// don't get slammed
//
// We want similar functionality from the web frontend so most of the logic
// here is broken out into a common function
#[get("/sites?<tag>&<before>&<after>&<count>&<offset>&<sorting>")]
pub fn sites(tag: Option<String>,
         before: Option<Date>, 
         after: Option<Date>, 
         count: Option<i64>, 
         offset: Option<i64>,
         sorting: Option<Sorting>,
         pooled_conn: DbConn,
         ) -> Result<Json<Vec<Site>>,Status> {

    use db::sites::dsl::*;

    // User is already authed and everything so just add the site into the database

    let connection = &*pooled_conn;
    let mut query = sites.into_boxed();

    // Filter based on all of the potential options a user can provide
    //
    if let Some(tag) = tag {
        query = query.filter(tags.contains(vec![tag]));
    };

    if let Some(before) = before {
        let before_date = NaiveDate::parse_from_str(&before,"%Y-%m-%d")
            .map_err(|_| Status::InternalServerError)?;
        query = query.filter(date(post_date).lt(before_date));
    };

    if let Some(after) = after {
        let after_date = NaiveDate::parse_from_str(&after,"%Y-%m-%d")
            .map_err(|_| Status::InternalServerError)?;
        query = query.filter(date(post_date).gt(after_date));
    };

    if let Some(count) = count {
        query = query.limit(count);
    };

    if let Some(offset) = offset {
        query = query.offset(offset);
    };

    if let Some(sorting) = sorting {
        use Sorting::*;
        query = match sorting {
            Newest => query.order(post_date.desc()),
            Oldest => query.order(post_date.asc()),
            // NOTE: This can't get implemented as part of the query
            // We just have to scramble the results afterwards
            Random => query,
            Alphabetical => query.order(title.asc()),
            ReverseAlphabetical => query.order(title.desc())
        }
    } else {
        query = query.order(id.asc());
    };

    let mut site_list = query.load(connection)
        .map_err(|_| Status::InternalServerError)?;

    // Randomize the order if the user selected random sorting
    if let Some(Sorting::Random) = sorting {
        site_list.shuffle(&mut rand::thread_rng());
    }

    return Ok(Json(site_list))
}

// These are the different kinds of sorting you can do
// for sorting method x, x is first
#[derive(Debug,FromFormValue,Copy,Clone)]
pub enum Sorting {
    Newest,
    Oldest,
    Random,
    Alphabetical,
    ReverseAlphabetical
}

#[put("/site/<site_id>", format= "application/json", data = "<site>")]
pub fn put_site(pooled_conn: DbConn, _key: Token, site_id: i32, site: Json<OptionalPostSite>) -> Result<Json<Site>,Status> {

    let connection = &*pooled_conn;

    // Fetch the site we want to update
    let mut old_site: Site = maybe! {{
        use db::sites::dsl::*;
        sites.filter(id.eq(site_id))
        .first(connection)}};

    // And set the fields of this site if the user supplied anything different
    if let Some(url) = &site.url {
        old_site.url = url.clone();
    };

    if let Some(description) = &site.description {
        old_site.description = description.clone();
    };

    if let Some(title) = &site.title {
        old_site.title = title.clone();
    }

    if let Some(tags) = &site.tags {
        let mut tags = tags.clone();
        for tag in tags.iter_mut() {
            *tag=tag.to_lowercase();
        }

        old_site.tags = tags;
    }

    // And finally, update the value in the table
    let result = maybe!{{
        use db::sites::dsl::*;
        diesel::update(sites.filter(id.eq(site_id)))
        .set(&old_site)
        .get_result(connection)}};

    return Ok(Json(result))
}
#[get("/site/<site_id>")]
pub fn get_site(pooled_conn: DbConn, site_id: i32) -> Result<Json<Site>,Status> {
    let connection = &*pooled_conn;

    // Fetch the site
    let site: Site = maybe! {{
        use db::sites::dsl::*;
        sites.filter(id.eq(site_id))
        .first(connection)}};

    return Ok(Json(site))
}
#[post("/site/<site_id>/tags/add/<tag>")]
pub fn add_tag(pooled_conn: DbConn, _key: Token, site_id: i32, tag: String) -> Result<Json<Site>,Status> {

    let connection = &*pooled_conn;

    // Fetch the site we want to update
    let mut old_site: Site = maybe! {{
        use db::sites::dsl::*;
        sites.filter(id.eq(site_id))
        .first(connection)}};

    // Add the new tag to the tags array
    old_site.tags.push(tag.to_lowercase());

    // And finally, update the value in the table
    let result = maybe!{{
        use db::sites::dsl::*;
        diesel::update(sites.filter(id.eq(site_id)))
        .set(&old_site)
        .get_result(connection)}};

    return Ok(Json(result))
}

#[post("/site/<site_id>/tags/remove/<tag>")]
pub fn remove_tag(pooled_conn: DbConn, _key: Token, site_id: i32, tag: String) -> Result<Json<Site>,Status> {

    let connection = &*pooled_conn;

    // All tags in the web directory are in lowercase
    // so this just makes it easier to select them
    let tag = tag.to_lowercase();

    // Fetch the site we want to update
    let mut old_site: Site = maybe! {{
        use db::sites::dsl::*;
        sites.filter(id.eq(site_id))
        .first(connection)}};

    // Add the new tag to the tags array
    old_site.tags = old_site.tags
        .iter()
        .cloned()
        .filter(|old_tag| *old_tag != tag)
        .collect();

    // And finally, update the value in the table
    let result = maybe!{{
        use db::sites::dsl::*;
        diesel::update(sites.filter(id.eq(site_id)))
        .set(&old_site)
        .get_result(connection)}};

    return Ok(Json(result))

}
#[delete("/site/<site_id>")]
pub fn remove_site(pooled_conn: DbConn, _key: Token, site_id: i32) -> Result<Json<Site>,Status> {

    let connection = &*pooled_conn;

    // Delete the given site from the database
    let result = maybe!{{
        use db::sites::dsl::*;
        diesel::delete(sites.filter(id.eq(site_id)))
        .get_result(connection)}};

    return Ok(Json(result))

}
