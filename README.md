# Terra 

Terra is a simple, collaborative web directory. The server has both a HTML
interface, to give a nice UI for people browsing, and a REST API. There is a
provided shell script, `terra.sh`, that makes use of this API. The documentation
is pretty bad right now but reading this tool is a good way to understand the
API.

To run Terra yourself you'll need to setup a postgres database, to store the
users and sites. Stick your credentials for this in a `.env` file in the main
terra directory.

```
 $ cat .env
DATABASE_URL=postgres://db_user:db_pass@your.db.server/terra
```
