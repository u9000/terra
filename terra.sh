# This is a simple command-line client for the Terra API

set -e

SERVER=https://terra.finzdani.net/api
AUTH_FILE=~/.terra_auth

if [[ -f $AUTH_FILE ]] ; then
    TOKEN=$(cat $AUTH_FILE)
fi

help() {
cat << EOM
the terra CLI client.

Usage:
    terra.sh COMMAND

Commands:
    sites                          View all sites as JSON. jq is recommended
    register <code>                Register a new account using a registration code
    login                          Login and authenticate all other commands
    new_code                       Create a new registration code
    <add|new>                      Add a new JSON-encoded site
    <get|site>      <id>           Get a site from the database by id
    <remove|delete> <id>           Remove a site from the database
    <update|edit>   <id>           Update a site by providing
    add_tag         <id> <tag>     Add a new tag to a site
    remove_tag      <id> <tag>     Remove a tag from a site
    help                           Show this screen

SITE:
    { 
      "url": String,
      "title": String,
      "description: String,
      "tags": [String]
    }
EOM
}

prompt() {
    read -rp "Username: " user 
    read -rsp "Password: " pass 
}

command=$1

case $command in 
    "help"|"--help")
        help
        ;;
    "sites")
        curl -sS "$SERVER/sites"
        echo
        ;;
    "register")
        prompt
        curl -sSH "Content-Type: application/json" "$SERVER/register/$2" -d "{\"username\": \"$user\", \"password\": \"$pass\"}"
        echo
        ;;
    "login")
        prompt
        token=$(curl -fsS -H "Content-Type: application/json" "$SERVER/auth" -d "{\"username\": \"$user\", \"password\": \"$pass\"}")
        echo "$token" > $AUTH_FILE
        chmod 600 $AUTH_FILE
        echo "Login succesful"
        ;;
    "new_code")
        curl -sSH "X-AUTH: $TOKEN" $SERVER/new_code
        echo
        ;;
    "add"|"new")
        TEMP=$(mktemp /tmp/terra.new_site.XXXXXX)
        tee "$TEMP.1" << EOF > "$TEMP"
{ 
  "url": "",
  "title": "",
  "description": "",
  "tags": ["",""]
}
EOF
        $EDITOR "$TEMP"

        if cmp --silent "$TEMP" "$TEMP.1"; then
            echo "Empty site, please fill in all required fields and try again."
        else
            curl -sSH "X-AUTH: $TOKEN" -H "Content-Type: application/json" $SERVER/add_site -d @"$TEMP"
            echo
            rm "$TEMP" "$TEMP.1"
        fi
        ;;
    "get"|"site")
        curl -sS $SERVER/site/"$2"
        echo
        ;;
    "remove"|"delete")
        curl -sSH "X-AUTH: $TOKEN" $SERVER/site/"$2" -X DELETE
        echo
        ;;
    "update"|"edit")
        TEMP=$(mktemp /tmp/terra.update_site.XXXXXX)
        curl -sS $SERVER/site/"$2" | jq '{ url, description, title, tags }' > "$TEMP"
        $EDITOR "$TEMP"
        curl -sSH "X-AUTH: $TOKEN" -H "Content-Type: application/json" -X PUT $SERVER/site/"$2" -d @"$TEMP"
        echo
        ;;
    "add_tag")
        curl -sSH "X-AUTH: $TOKEN" -X POST $SERVER/site/"$2"/tags/add/"$3"
        echo
        ;;
    "remove_tag")
        curl -sSH "X-AUTH: $TOKEN" -X POST $SERVER/site/"$2"/tags/remove/"$3"
        echo
        ;;
    # If we didn't get any valid commands just show the help entry
    # and the given command
    *)
        echo "Invalid command: $command"
        help
        ;;

esac

